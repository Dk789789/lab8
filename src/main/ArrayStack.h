/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017
 *
 * @file    ArrayStack.h
 * @authors Jim Daehn <jdaehn@missouristate.edu>
 * @brief   Specification for an array-based implementation of the Stack interface.
 */
#ifndef LAB08_ARRAY_STACK_H
#define LAB08_ARRAY_STACK_H

#include "Stack.h"

namespace csc232 {
    /**
     * An Array-based implementation of the Stack interface.
     * @tparam T the type of items stored in this stack
     */
    template<typename T>
    class ArrayStack : public Stack<T> {
    public:
        /**
         * Default constructor.
         */
        ArrayStack();

        /**
         * Determines whether this stack is empty.
         *
         * @return True if this stack is empty, false otherwise.
         */
        bool isEmpty() const override;

        /**
         * Adds a new entry to the top of this stack.
         *
         * @post If the operation was successful, newEntry is at the top of the stack.
         * @param newEntry The object to be added as a new entry.
         * @return True if the addition is successful or false if not.
         */
        bool push(const T &newEntry) throw(PrecondViolatedExcept) override;

        /**
         * Removes the top of this stack.
         *
         * @post If the operation was successful, the top of the stack has been removed.
         * @return True if the removal is successful or false if not.
         */
        bool pop() override;

        /**
         * Returns a copy of the top of this stack.
         *
         * @pre The stack is not empty.
         * @post A copy of the top of the stack has been returned, and the stack is unchanged.
         * @return A copy of the top of the stack.
         */
        T peek() const throw(PrecondViolatedExcept) override;

        /**
         * Destroys this stack and frees its assigned memory.
         */
        virtual ~ArrayStack() { /* no op */ }

    private:
        static const int DEFAULT_CAPACITY = 100;
        T items[DEFAULT_CAPACITY];
        int top;
    }; // class ArrayStack

} // namespace csc232

#include "ArrayStack.cpp"

#endif //LAB08_ARRAY_STACK_H

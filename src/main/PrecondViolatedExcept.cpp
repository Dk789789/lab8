/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017
 *
 * @file    PrecondViolatedExcept.cpp
 * @authors Jim Daehn <jdaehn@missouristate.edu>
 * @brief   Definition for our custom pre-condition violated exception.
 */

#include "PrecondViolatedExcept.h"

namespace csc232 {
    PrecondViolatedExcept::PrecondViolatedExcept(const std::string &message)
            : std::logic_error("Precondition Violated Exception: " + message) {
    }
}